# # Topics
#
# * modules
# * strings
#
# # Pig Latin
#
# Pig Latin is a made-up children's language that's intended to be
# confusing. It obeys a few simple rules (below) but when it's spoken
# quickly it's really difficult for non-children (and non-native
# speakers) to understand.
#
# Rule 1: If a word begins with a vowel sound, add an "ay" sound to
# the end of the word.
#
# Rule 2: If a word begins with a consonant sound, move it to the end
# of the word, and then add an "ay" sound to the end of the word.
#
# (There are a few more rules for edge cases, and there are regional
# variants too, but that should be enough to understand the tests.)
#
# See <http://en.wikipedia.org/wiki/Pig_latin> for more details.
def translate(s)
  s.split.map do |word|
    vowel_idx = first_vowel_index(word)
    if vowel_idx.zero?
      word + 'ay'
    else
      word[vowel_idx..-1] + word[0..(vowel_idx - 1)] + 'ay'
    end
  end.join(" ")

end

def first_vowel_index(word)
  vowel = %w(a e i o u)
  word.chars.each_index do |idx|
    if vowel.include?(word[idx].downcase)
      if idx >= 1 && word[(idx - 1)..idx] == 'qu'
        next
      else
        return idx
      end
    end
  end
  nil
end
