# # Simon Says
#
# ## Topics
#
# * functions
# * strings
# * default parameter values
#
# ## Hints
#
# When you make the second `repeat` test pass, you might break the
# **first**

def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, number = 2)
  string_array = Array.new(number, string)
  string_array.join(" ")
end

def start_of_word(word,n)
  word[0...n]
end

def first_word(string)
  string.split[0]
end

def titleize(string)
  little_words = ['a', 'an', 'the', 'and', 'over']
  string.split.map.with_index do |word, idx|
    if idx == 0
      word.capitalize
    elsif little_words.include?(word)
      word
    else
      word.capitalize
    end
  end.join(" ")
end
